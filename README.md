# React Application - README

This README file provides instructions on how to run a ``pharmacyClient`` stored in GitLab. Follow the steps below to get started.
This service interact with the user, acts as an interface to the inventory management system

## Prerequisites

Before running the application, ensure that you have the following installed:

- Nodejs  version v16.13.2 or higher
- NPM (node package manager) 8.1.2 or higher
- Git

## Clone the Repository

1. Open a terminal or command prompt.
2. Clone the `https://gitlab.com/learnly2/pharmacyclient.git` repository using the `git clone` command:

## Run the Application

1. Navigate to the project root directory
2. Run `npm install` command to install the project dependencies
3. Run `npm start` to run the application on `dev-server` for testing purpose

## Access the Application

Once the application is running, you can access it using a web browser. By default, the application runs on `http://localhost:3000`. 
Use `Google Chrome` for best user experience.

## Configuration
By default, API calls are made to `http://${window.location.hostname}:9347`. This is a basic configuration for local development and testing purposes. 
In production environment, appropriate `[environment].env` files should be created and modifications should be made to `package.json` file i.e
```
"start:staging": "env-cmd -f staging.env react-scripts start",
"build:staging": "env-cmd -f staging.env react-scripts build",
```

Also, the base url to the gateway should be provided in the `.env file` i.e `REACT_APP_BASE_URL_ENV = https://pharma.com` but for this purpose,
the base url will be statically configured in Axios create instance which is in `rootdir > src > util > axios-util.ts`

```
export const axiosInstance = axios.create({
    baseURL: `http://${window.location.hostname}:9347`,
    timeout: 10000,
    headers: {'Content-Type': 'application/json'}
});
```

**Note** The port number `9347` points all api calls to the `gatewayService` which routes requests to backend service.
## Conclusion

You have successfully cloned, built, and run the React application stored in GitLab. In case of any challenge please reach out.
