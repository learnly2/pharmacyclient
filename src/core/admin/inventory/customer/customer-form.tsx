import {Button, Divider, Form, Input, Spin} from "antd";
import {useAppDispatch, useAppSelector} from "../../../../redux/store/hooks";
import {ApiErrorComponent} from "../../../../util/api-error-component";
import {useEffect} from "react";
import {
    addCustomer,
    fetchCustomer,
    handleCustomerError,
} from "../../../../redux/slice/core/admin/inventory/customer/customer-slice";
import {
    AddCustomerRequest
} from "../../../../schema/app-data-schema";
import {checkNotFoundError} from "../../../../util/helpers";

interface FormTypes {
    phonenumber: string
    fullName: string
}

export const CustomerForm = () => {
    const {error, loading} = useAppSelector(state => state.customerReducer);
    const [form] = Form.useForm<FormTypes>();
    const dispatch = useAppDispatch();

    const onFinish = ({phonenumber, fullName}: FormTypes) => {
        const addCustomerRequest: AddCustomerRequest = {
            fullName: fullName,
            phoneNumber: phonenumber
        }
        dispatch(addCustomer(addCustomerRequest));
    }

    const onFinishFailed = (errorInfo: any) => {
        dispatch(handleCustomerError(errorInfo.message));
    }

    return (
        <Spin spinning={loading}>
            <Divider orientation="left">Customer detail</Divider>
            <ApiErrorComponent error={checkNotFoundError(String(error))}/>
            <br/>
            <Form
                name="basic"
                initialValues={{phonenumber: "", fullName: ""}}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                layout={'vertical'}
                preserve={false}
                form={form}
                autoComplete="off">
                <Form.Item
                    labelAlign={'left'}
                    label="Phone number"
                    name="phonenumber"
                    rules={[
                        {required: true, message: 'Phone number is required!'},
                        {type: 'string', message: 'Invalid input!'},
                        {max: 10, message: 'Maximum characters is 10!'}
                    ]}>
                    <Input placeholder={'07..'} max={'10'}/>
                </Form.Item>
                <Form.Item
                    labelAlign={'left'}
                    label="Full name"
                    name="fullName"
                    rules={[
                        {required: true, message: 'Name is required!'},
                        {type: 'string', message: 'Invalid input!'},
                        {max: 100, message: 'Maximum characters is 100!'}
                    ]}>
                    <Input/>
                </Form.Item>
                {error === 'CNF-001' ? <Form.Item>
                    <Button type="primary" htmlType="submit" size={'middle'} block={true}>
                        Add customer
                    </Button>
                </Form.Item> : null}
            </Form>
        </Spin>
    )
}
