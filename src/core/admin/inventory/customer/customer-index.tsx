import {Button, Divider, Popover, Spin, Typography} from "antd";
import {CustomerForm} from "./customer-form";
import {ApiErrorComponent} from "../../../../util/api-error-component";
import {checkNotFoundError} from "../../../../util/helpers";
import Search from "antd/es/input/Search";
import {useAppDispatch, useAppSelector} from "../../../../redux/store/hooks";
import {FetchCustomerRequest} from "../../../../schema/app-data-schema";
import {fetchCustomer, handleCustomerError} from "../../../../redux/slice/core/admin/inventory/customer/customer-slice";
import {useEffect} from "react";

const {Title} = Typography;
export const CustomerIndex = () => {
    const {customer, error, loading, customerId} = useAppSelector(state => state.customerReducer);
    const dispatch = useAppDispatch();

    const onSearch = (value: string) => {
        if (value !== undefined && value.length === 10 && !customerId) {
            const customerRequest: FetchCustomerRequest = {
                phn: value,
                id: ""
            }
            dispatch(fetchCustomer(customerRequest))
        } else {
            dispatch(handleCustomerError(""))
        }
    }

    useEffect(() => {
        if (customerId) {
            const customerRequest: FetchCustomerRequest = {
                phn: "",
                id: customerId
            }
            dispatch(fetchCustomer(customerRequest))
        }
    }, [dispatch, customerId])


    return (
        <Spin spinning={loading}>
            <Divider orientation="left">Customer detail</Divider>
            <ApiErrorComponent error={checkNotFoundError(String(error))}/>
            <br/>
            <Search
                placeholder="Search by phone number 07.."
                onSearch={onSearch}
                enterButton
                defaultValue={customer.phoneNumber}
                maxLength={10}/>
            <br/>
            <Title level={3}>{error === 'CNF-001' ? "" : customer.fullName}</Title>
            <br/>
            {error === 'CNF-001' ?
                <Popover content={<CustomerForm/>} title="Title" trigger="click">
                    <Button type="primary" block={true}>Add Customer</Button>
                </Popover> : null}
        </Spin>
    )
}
