import {Col, Row} from "antd";
import {CartActions} from "./cart-actions";
import {CartItems} from "./cart-items";
import {CustomerIndex} from "../customer/customer-index";

export const Cart = () => {
    return (
        <Row justify={'space-evenly'}>
            <Col sm={22} xs={22} md={22} lg={14}>
                <CartItems/>
            </Col>
            <Col sm={22} xs={22} md={22} lg={8}>
                <CustomerIndex/>
                <CartActions/>
            </Col>
        </Row>
    )
}
