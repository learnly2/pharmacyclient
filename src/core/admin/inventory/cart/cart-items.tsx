import {Button, Divider, InputNumber, List, Space, Spin, Statistic, Tooltip} from "antd";
import {ApiErrorComponent} from "../../../../util/api-error-component";
import {MinusOutlined} from "@ant-design/icons";
import {calculateCartItemPrice, currencyFormatter} from "../../../../util/helpers";
import {useAppDispatch, useAppSelector} from "../../../../redux/store/hooks";
import {useEffect} from "react";
import {
    exemptBillItem,
    fetchBillItem,
    removeCartItem,
    updateCartItem
} from "../../../../redux/slice/core/admin/inventory/cart/cart-item-slice";
import {UpdatedQuantity} from "../../../../schema/app-data-schema";

export const CartItems = () => {
    const {items, billId, loading, error, billComplete} = useAppSelector(state => state.cartItemReducer);
    const dispatch = useAppDispatch();

    useEffect(() => {
        if (billId) {
            dispatch(fetchBillItem({billId: billId}))
        }
    }, [dispatch, billId])

    const handleQuantityChange = (value: number, id: string) => {
        const updatedQuantity: UpdatedQuantity = {
            selectedQuantity: value,
            id: id
        }
        dispatch(updateCartItem(updatedQuantity));
    }

    const handleItemRemoval = (id: string) => {
        dispatch(removeCartItem(id));

        if (billId) {
            dispatch(exemptBillItem({billId: billId, inventoryId: id}))
        }
    }

    return (
        <Spin spinning={loading}>
            <ApiErrorComponent error={error}/>
            <br/>
            <List
                itemLayout="vertical"
                size="large"
                dataSource={items}
                renderItem={(item) => (
                    <List.Item
                        key={item.id}
                        extra={
                            <Tooltip title="Remove item">
                                <Button
                                    disabled={billComplete}
                                    onClick={() => handleItemRemoval(item.id)}
                                    shape="circle"
                                    icon={<MinusOutlined/>}
                                    danger={true}/>
                            </Tooltip>
                        }>
                        <List.Item.Meta
                            title={item.name}
                        />
                        <Space direction={'vertical'}>
                            <InputNumber
                                size={'large'}
                                addonBefore={'Quantity'}
                                disabled={billComplete}
                                min={1}
                                max={billComplete ? item.selectedQuantity : item.quantity}
                                defaultValue={item.selectedQuantity}
                                onChange={(value) => handleQuantityChange(Number(value), item.id)}
                                addonAfter={currencyFormatter.format(item.price * item.selectedQuantity)}/>
                        </Space>
                    </List.Item>
                )}
            />
            <Divider/>
            <Statistic title="Total Amount" value={currencyFormatter.format(calculateCartItemPrice(items))}/>
        </Spin>
    )
}
