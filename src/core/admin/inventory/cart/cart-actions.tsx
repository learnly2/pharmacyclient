import {Button, Space, Spin} from "antd";
import {CheckOutlined, PlusOutlined, PrinterOutlined} from "@ant-design/icons";
import {useAppDispatch, useAppSelector} from "../../../../redux/store/hooks";
import {BillItemRequest, CreateBillRequest} from "../../../../schema/app-data-schema";
import {calculateCartItemPrice} from "../../../../util/helpers";
import {createBill} from "../../../../redux/slice/core/admin/inventory/cart/create-bill-slice";
import {ApiErrorComponent} from "../../../../util/api-error-component";
import {completeBill, hydrateItemError} from "../../../../redux/slice/core/admin/inventory/cart/cart-item-slice";

export const CartActions = () => {
    const {customer} = useAppSelector(state => state.customerReducer);
    const {items, billId, billComplete} = useAppSelector(state => state.cartItemReducer);
    const {error, loading} = useAppSelector(state => state.createBillReducer);
    const dispatch = useAppDispatch();

    const handlePrint = () => {
        window.print();
    }

    const handleCreateBill = () => {
        const billItems: BillItemRequest[] = [];
        let isValid = true

        items.forEach(i => {
            if (i.selectedQuantity > i.quantity) {
                isValid = false;
            } else {
                billItems.push({
                    quantity: i.selectedQuantity,
                    price: i.price * i.selectedQuantity,
                    inventoryId: i.id
                })
            }
        });

        const createBillRequest: CreateBillRequest = {
            billId: billId,
            totalPrice: calculateCartItemPrice(items),
            customerId: customer.id,
            items: billItems
        }

        if (isValid) {
            dispatch(createBill(createBillRequest));
            dispatch(hydrateItemError(""));
        } else {
            dispatch(hydrateItemError("One or more items are out of stock"));
        }
    }

    const handleCompleteBill = () => {
        dispatch(completeBill({billId: billId}))
    }

    return (
        <Spin spinning={loading}>
            <ApiErrorComponent error={error}/>
            <br/>

            <Space direction={'horizontal'} align={'center'} wrap>
                <Button type="primary" icon={<PlusOutlined/>} disabled={!customer.id || billComplete || items.length < 1}
                        onClick={handleCreateBill}>
                    Add bill
                </Button>
                <Button type="text" icon={<PrinterOutlined/>} onClick={handlePrint} disabled={!customer.id || !billId}>
                    Print bill
                </Button>
                <Button type="default" icon={<CheckOutlined/>} disabled={!customer.id || !billId || billComplete}
                        onClick={handleCompleteBill}>
                    Complete bill
                </Button>
            </Space>
        </Spin>
    )
}
