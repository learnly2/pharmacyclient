import {Drawer} from "antd";
import {useAppDispatch, useAppSelector} from "../../../../redux/store/hooks";
import {Cart} from "./cart";
import {
    handleResetCustomer,
    hydrateCustomerId
} from "../../../../redux/slice/core/admin/inventory/customer/customer-slice";
import {
    handleCartDrawer,
    hydrateBillId,
    hydrateBillStatus,
    resetCartItem
} from "../../../../redux/slice/core/admin/inventory/cart/cart-item-slice";
import {FC} from "react";

interface CartDrawerProps {
    clearState: boolean
}

export const CartDrawer: FC<CartDrawerProps> = ({clearState}) => {
    const {openDrawer} = useAppSelector(state => state.cartItemReducer);
    const dispatch = useAppDispatch();

    const closeDrawer = () => {
        dispatch(handleCartDrawer(false));
        dispatch(hydrateBillStatus(false));

        if (clearState) {
            dispatch(handleResetCustomer());
            dispatch(resetCartItem());
            dispatch(hydrateBillId(""));
            dispatch(hydrateCustomerId(""));
        }
    }

    return (
        <Drawer
            title="Cart items"
            onClose={closeDrawer}
            open={openDrawer}
            placement={'bottom'}
            height={'100dvh'}
            destroyOnClose={true}>
            <Cart/>
        </Drawer>
    )
}
