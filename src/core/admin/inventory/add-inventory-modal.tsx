import {useAppDispatch, useAppSelector} from "../../../redux/store/hooks";
import {Button, Form, Input, InputNumber, Modal, Spin} from "antd";
import {ApiErrorComponent} from "../../../util/api-error-component";
import {useEffect} from "react";
import {AddInventoryRequest} from "../../../schema/app-data-schema";
import {
    addInventoryItem, handleInventoryId, handleInventoryModal, handleNameInput, handlePriceInput, handleQuantityInput,
    inventoryInputError
} from "../../../redux/slice/core/admin/inventory/add-inventory-modal-slice";

interface FormTypes {
    name: string
    quantity: number
    price: number
}

export const AddInventoryModal = () => {
    const {
        error,
        loading,
        inventoryId,
        open,
        name,
        quantity,
        price
    } = useAppSelector(state => state.addInventoryModalReducer);
    const [form] = Form.useForm<FormTypes>();
    const dispatch = useAppDispatch();

    useEffect(() => {
        return () => {
            form.resetFields();
        };
    }, [form]);

    const handleCancel = () => {
        dispatch(handleInventoryModal(false));
        dispatch(handleInventoryId(""));
        dispatch(handleNameInput(""));
        dispatch(handleQuantityInput(1));
        dispatch(handlePriceInput(null));
    }

    const onFinish = ({name, quantity, price}: FormTypes) => {
        const addInventoryRequest: AddInventoryRequest = {
            inventoryId: inventoryId,
            name: name,
            quantity: quantity,
            price: price
        }
        dispatch(addInventoryItem(addInventoryRequest));
    }

    const onFinishFailed = (errorInfo: any) => {
        dispatch(inventoryInputError(errorInfo.message));
    }

    return (
        <Modal
            title="Enter item detail"
            open={open}
            footer={null}
            closable={true}
            maskClosable={true}
            destroyOnClose={true}
            onCancel={handleCancel}>
            <Spin spinning={loading}>
                <ApiErrorComponent error={error}/>
                <br/>

                <Form
                    name="basic"
                    initialValues={{name: name, quantity: quantity, price: price}}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    layout={'vertical'}
                    preserve={false}
                    form={form}
                    autoComplete="off">
                    <Form.Item
                        labelAlign={'left'}
                        label="Name"
                        name="name"
                        rules={[
                            {required: true, message: 'Name is required!'},
                            {type: 'string', message: 'Invalid input!'},
                            {max: 200, message: 'Maximum characters is 200!'}
                        ]}>
                        <Input placeholder={'Item name'}/>
                    </Form.Item>
                    <Form.Item
                        labelAlign={'left'}
                        label="Quantity"
                        name="quantity"
                        rules={[
                            {required: true, message: 'Quantity is required!'},
                            {type: 'number', message: 'Invalid input!'}
                        ]}>
                        <InputNumber style={{width: '100%'}} placeholder={'Available quantity'}/>
                    </Form.Item>
                    <Form.Item
                        labelAlign={'left'}
                        label="Price / unit"
                        name="price"
                        rules={[
                            {required: true, message: 'Unit price is required!'},
                            {type: 'number', message: 'Invalid input!'}
                        ]}>
                        <InputNumber style={{width: '100%'}} placeholder={'Price per unit'}/>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" size={'middle'} block={true}>
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </Spin>
        </Modal>
    )
}
