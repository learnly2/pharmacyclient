import {Button, Card, Col, Popconfirm, Row, Space, Spin, Tooltip, Typography} from "antd";
import {useAppDispatch, useAppSelector} from "../../../redux/store/hooks";
import {ApiErrorComponent} from "../../../util/api-error-component";
import {useEffect} from "react";
import {deleteInventoryItem, fetchInventoryItems} from "../../../redux/slice/core/admin/inventory/inventory-list-slice";
import {currencyFormatter, numberFormatter} from "../../../util/helpers";
import Meta from "antd/es/card/Meta";
import {DeleteOutlined, EditOutlined, ShoppingCartOutlined} from "@ant-design/icons";
import {CartItem, Inventory} from "../../../schema/app-data-schema";
import {addCartItem} from "../../../redux/slice/core/admin/inventory/cart/cart-item-slice";
import {AddInventoryModal} from "./add-inventory-modal";
import {
    handleInventoryId, handleInventoryModal,
    handleNameInput, handlePriceInput,
    handleQuantityInput
} from "../../../redux/slice/core/admin/inventory/add-inventory-modal-slice";

const {Text} = Typography;
export const InventoryItems = () => {
    const {loading, items, error} = useAppSelector(state => state.inventoryListReducer);
    const {items: cartItems} = useAppSelector(state => state.cartItemReducer);
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(fetchInventoryItems());
    }, [dispatch])

    const handleItemSelect = (item: Inventory) => {
        const cartItem: CartItem = {
            id: item.id,
            name: item.name,
            quantity: item.quantity,
            price: item.price,
            exempt: item.exempt,
            selectedQuantity: 1
        }

        dispatch(addCartItem(cartItem));
    }

    const handleEdit = (inventory: Inventory) => {
        dispatch(handleInventoryId(inventory.id));
        dispatch(handleNameInput(inventory.name));
        dispatch(handleQuantityInput(inventory.quantity));
        dispatch(handlePriceInput(inventory.price));
        dispatch(handleInventoryModal(true))
    }

    const confirm = (id: string) => {
        dispatch(deleteInventoryItem({inventoryId: id}))
    };


    return (
        <Spin spinning={loading}>
            <ApiErrorComponent error={error}/>

            <Row gutter={[16, 16]}>
                {items.map((i, index) => (
                    <Col xs={22} sm={22} md={12} lg={8}>
                        <Card
                            title={i.name}
                            key={index}
                            extra={<Button
                                type={"text"}
                                onClick={() => handleItemSelect(i)}
                                danger={cartItems.some(c => c.id === i.id)}
                                icon={<ShoppingCartOutlined/>}
                                disabled={i.quantity < 1}>
                                Add to cart
                            </Button>}
                            actions={[
                                <Tooltip title="Edit item">
                                    <Button
                                        shape="circle"
                                        onClick={() => handleEdit(i)}
                                        icon={<EditOutlined/>}/>
                                </Tooltip>,
                                <Popconfirm
                                    title="Delete item"
                                    description="Are you sure to delete this item?"
                                    onConfirm={() => confirm(i.id)}
                                    okText="Yes"
                                    cancelText="No">
                                    <Tooltip title="Remove item">
                                        <Button shape="circle" icon={<DeleteOutlined/>} danger={true}/>
                                    </Tooltip>
                                </Popconfirm>

                            ]}>
                            <Meta
                                description={
                                    <Space direction={'vertical'}>
                                        <Text type={'secondary'}
                                              key={"quantity"}>Quantity: &emsp; {numberFormatter.format(i.quantity)}</Text>
                                        <Text type={'secondary'} key={"price"}>Price / unit: &emsp;
                                            {currencyFormatter.format(i.price)}</Text>
                                    </Space>
                                }
                            />
                        </Card>
                    </Col>
                ))}
                <AddInventoryModal/>
            </Row>
        </Spin>
    )
}
