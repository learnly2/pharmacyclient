import {Button, Col, FloatButton, Row, Typography} from "antd"
import {InventoryItems} from "./inventory-items";
import {useAppDispatch, useAppSelector} from "../../../redux/store/hooks";
import {handleInventoryModal} from "../../../redux/slice/core/admin/inventory/add-inventory-modal-slice";
import {ShoppingCartOutlined} from "@ant-design/icons";
import {CartDrawer} from "./cart/cart-drawer";
import {handleCartDrawer} from "../../../redux/slice/core/admin/inventory/cart/cart-item-slice";

const {Title} = Typography;
export const InventoryIndex = () => {
    const {items} = useAppSelector(state => state.cartItemReducer);
    const dispatch = useAppDispatch();

    const handleAddInventory = () => {
        dispatch(handleInventoryModal(true))
    }

   const openCartDrawer = () => {
       dispatch(handleCartDrawer(true))
   }

    return (
        <Row justify={'center'}>
            <Col span={22}>
                <Title level={2}>Inventory</Title>
                <Button type="primary" onClick={handleAddInventory}>Add inventory item</Button>
                <br/>
                <br/>
            </Col>
            <Col sm={22} md={22} lg={22} xl={22} offset={1} xs={22}>
                <InventoryItems/>
            </Col>
            <FloatButton
                type="primary"
                shape="circle"
                badge={{size: "default", count: items.length, showZero: true}}
                onClick={openCartDrawer}
                icon={<ShoppingCartOutlined/>}/>
            <CartDrawer clearState={false}/>
        </Row>
    )
}
