import {useAppDispatch, useAppSelector} from "../../../redux/store/hooks";
import {useEffect} from "react";
import {fetchAllBill} from "../../../redux/slice/core/admin/bills/bill-list-slice";
import {ColumnsType} from "antd/es/table";
import {ExtendedBill} from "../../../schema/app-data-schema";
import {Spin, Table, Typography} from "antd";
import {ApiErrorComponent} from "../../../util/api-error-component";
import {currencyFormatter} from "../../../util/helpers";
import {
    handleCartDrawer,
    hydrateBillId,
    hydrateBillStatus
} from "../../../redux/slice/core/admin/inventory/cart/cart-item-slice";
import {hydrateCustomerId} from "../../../redux/slice/core/admin/inventory/customer/customer-slice";

export const BillList = () => {
    const {error, loading, bills} = useAppSelector(state => state.billListReducer);
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(fetchAllBill());
    }, [dispatch])

    const handleEdit = (id: string) => {
        const bill = bills.find(b => b.id === id);

        if (!bill) {
            return;
        }

        dispatch(hydrateBillId(id));
        dispatch(handleCartDrawer(true));
        dispatch(hydrateCustomerId(bill.customerId))
        dispatch(hydrateBillStatus(bill.complete))
    }

    const columns: ColumnsType<ExtendedBill> = [
        {
            title: 'Customer',
            dataIndex: 'fullName',
            key: 'fullName'
        },
        {
            title: 'Total price',
            dataIndex: 'totalPrice',
            key: 'totalPrice',
            render: (price) => currencyFormatter.format(price)
        },
        {
            title: 'Complete',
            dataIndex: 'complete',
            key: 'complete',
            render: (status) => status ? "True" : "False"
        },
        {
            title: 'Action',
            dataIndex: 'id',
            key: 'id',
            render: (id) => <Typography.Link onClick={() => handleEdit(id)}>Detail</Typography.Link>
        }
    ];

    return (
        <Spin spinning={loading}>
            <ApiErrorComponent error={error}/>
            <Table
                columns={columns}
                dataSource={bills}
                rowKey={'id'}
                pagination={false}/>
        </Spin>
    )
}
