import {Col, Row, Typography} from "antd";
import {BillList} from "./bill-list";
import {CartDrawer} from "../inventory/cart/cart-drawer";
import {useMemo} from "react";
import {generateRandomAlphabetKey} from "../../../util/helpers";

const {Title}=Typography;
export const BillIndex = () => {
    const keyMemo = useMemo(() => generateRandomAlphabetKey(), [])

    return (
        <Row justify={'center'}>
            <Col flex={3} offset={1}>
                <Title level={2}>Bills</Title>
            </Col>
            <Col sm={22} md={22} lg={22} xl={22} offset={1}>
                <BillList/>
            </Col>
            <CartDrawer key={keyMemo} clearState={true}/>
        </Row>
    )
}
