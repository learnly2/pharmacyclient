import {useAppSelector} from "../../redux/store/hooks";
import {FC, useEffect} from "react";
import {Spin} from "antd";
import {LoadingOutlined} from "@ant-design/icons";
import {checkAdmin} from "../../util/helpers";
import {useNavigate} from "react-router-dom";

export const Redirect: FC = () => {
    const {roles, loggedIn} = useAppSelector(state => state.signInReducer);
    const navigate = useNavigate();

    useEffect(() => {
        if (checkAdmin(roles) && loggedIn) {
            navigate("/dashboard/admin", {replace: true})
        }
    }, [roles, navigate, loggedIn])

    return <Spin indicator={<LoadingOutlined style={{fontSize: 24}} spin/>}/>;
}
