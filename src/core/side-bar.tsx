import React, {FC} from "react";
import {useAppDispatch, useAppSelector} from "../redux/store/hooks";
import {Layout, Menu, MenuProps} from "antd";
import {
    AppstoreOutlined,
    HomeOutlined,
    LogoutOutlined,
    MenuFoldOutlined,
    MenuUnfoldOutlined, ScheduleOutlined
} from "@ant-design/icons";
import {Link} from "react-router-dom";
import {getRoleDashboardUrl} from "../util/helpers";
import {handleTopNavActiveItem, hydrateCollapsible} from "../redux/slice/core/sidebar-toggle-slice";
const { Sider} = Layout;

export const SideBar: FC = () => {
    const {sideBarOn, activeItem} = useAppSelector(state => state.sideBarToggleReducer);
    const {roles} = useAppSelector(state => state.signInReducer);
    const dispatch = useAppDispatch();
    type MenuItem = Required<MenuProps>['items'][number];

    const getItem = (
        label: React.ReactNode,
        key: React.Key,
        icon?: React.ReactNode,
        style?: React.CSSProperties,
        children?: MenuItem[],
        type?: 'group'
    ): MenuItem  => {
        return {
            key,
            icon,
            children,
            label,
            type,
            style
        } as MenuItem;
    }

    const items: MenuProps['items'] = [
        getItem(<Link to={getRoleDashboardUrl(roles)}>Dashboard</Link>, 'dashboard', <AppstoreOutlined />),
        getItem(<Link to={'/admin/bills'}>Bills</Link>, 'bills', <ScheduleOutlined />),
        getItem(<Link to={'/logout'}>Logout</Link>, 'logout', <LogoutOutlined />),
    ];

    const onClick: MenuProps['onClick'] = ({key}) => {
        if (key === 'logout') {
       //     dispatch(logout({refreshToken: refreshToken}));
        }
        dispatch(handleTopNavActiveItem(key));
    };

    const handleCollapsible = () => {
        dispatch(hydrateCollapsible())
    }

    return (
        <Sider
            collapsible
            trigger={sideBarOn ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
            onCollapse={handleCollapsible}
            collapsedWidth={0}
            breakpoint={'md'}
            collapsed={sideBarOn}
            >
            <Menu
                className={'core-slider-menu'}
                onClick={onClick}
                theme={'light'}
                mode={'inline'}
                defaultSelectedKeys={[activeItem as string]}
                items={items}
            />
        </Sider>
    )
};
