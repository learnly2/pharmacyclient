import {Layout} from "antd";
import { Outlet } from "react-router-dom";
import {SideBar} from "./side-bar";
import {LayoutHeader} from "./layout-header";

const { Content } = Layout;

export const CoreLayout = () => {
    return(
        <Layout hasSider={true} className={'core-layout'}>
            <SideBar/>
            <Layout>
                <LayoutHeader/>
                <Content>
                    <Outlet/>
                </Content>
            </Layout>
        </Layout>
    )
}
