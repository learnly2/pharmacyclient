import {FC} from "react";
import {useAppSelector} from "../redux/store/hooks";

interface ClientRouteProps {
    children: JSX.Element
}

export const ClientRoute: FC<ClientRouteProps> = ({children}) => {
    const {roles} = useAppSelector(state => state.signInReducer);
    return roles.includes('ROLE_CLIENT') ? children : null
};
