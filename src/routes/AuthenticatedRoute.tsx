import {Navigate} from "react-router-dom";
import React, {FC} from "react";
import {useAppSelector} from "../redux/store/hooks";

interface EntryRouteProps {
    children: JSX.Element
}

export const AuthenticatedRoute: FC<EntryRouteProps> = ({children}) => {
    const {loggedIn} = useAppSelector(state => state.signInReducer);
    return loggedIn ? children : <Navigate to={'/'} replace={true}/>;
};
