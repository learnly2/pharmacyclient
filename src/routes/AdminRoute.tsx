import {FC} from "react";
import {useAppSelector} from "../redux/store/hooks";

interface AdminRouteProps {
    children: JSX.Element
}

export const AdminRoute: FC<AdminRouteProps> = ({children}) => {
    const {roles} = useAppSelector(state => state.signInReducer);
    return roles.includes('ROLE_ADMIN') ? children : null
};
