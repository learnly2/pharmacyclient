import {FC} from "react";
import {useAppSelector} from "../redux/store/hooks";

interface DispatchRouteProps {
    children: JSX.Element
}

export const DispatchRoute: FC<DispatchRouteProps> = ({children}) => {
    const {roles} = useAppSelector(state => state.signInReducer);
    return roles.includes('ROLE_DRIVER') ? children : null
};
