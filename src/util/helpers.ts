import jwt_decode from "jwt-decode";
import {CartItem, TokenData} from "../schema/app-data-schema";
import {Base64} from "js-base64";

export const decodeJwtToken = (jwt: string): string => {
    return jwt_decode(jwt)
}

export const extractUserIdFromTokenObject = (decoded: any): string => {
    return decoded.sub;
}

export const extractRolesFromTokenObject = (decoded: any): string[] => {
    return decoded.realm_access.roles;
}

export const extractTokenData = (token: string): TokenData => {
    const decoded = decodeJwtToken(token);
    const roles = extractRolesFromTokenObject(decoded);
    const userId = extractUserIdFromTokenObject(decoded);
    return {roles, userId};
}

export const extractError = (error: string): string => {
    try {
        const errorObj = JSON.parse(error);
        return errorObj.error_description;
    } catch (e) {
        return error;
    }
}

export const getRoleDashboardUrl = (role: string[]): string => {
    return checkAdmin(role) ? "/dashboard/admin" : "";
}

export const checkAdmin = (roles: string[]) => {
    return roles.includes('ROLE_ADMIN');
}

export const updateStateOrCreate = (currentState: any, comparisonKey: string, changeValues: string[], payload: any) => {
    if (currentState.filter((i: any) => i[comparisonKey] === payload[comparisonKey]).length > 0) {

        currentState.forEach((i: any) => {
            if (i[comparisonKey] === payload[comparisonKey]) {
                changeValues.forEach((item: any) => {
                    i[item] = payload[item]
                });
            }
        });

        return currentState;
    } else {
        return [...currentState, payload]
    }
};

export const removeItemFromObjectList = (currentState: any, key: string, value: any) => {
    return currentState.filter((item: any) => item[key] !== value);
};

export const filterItemFromArray = (currentState: any, filterValue: any) => {
    return currentState.filter((i: any) => i !== filterValue);
};

export const getCookie = (cookieName: string | null) => {
    const cookieValue = document.cookie.split('; ').find(row => row.startsWith(cookieName || ''))?.split('=')[1] || '';
    if (cookieValue) {
        return JSON.parse(Base64.decode(cookieValue))
    } else {
        return '';
    }
};

export const currencyFormatter = new Intl.NumberFormat('en-KE', {
    style: 'currency',
    currency: 'KES',
    minimumFractionDigits: 2
});

export const numberFormatter = new Intl.NumberFormat('en-KE', {
    minimumFractionDigits: 2,
});

export const calculateCartItemPrice = (items: CartItem[]) => {
    return items.reduce((accumulator, item) => {
        const itemTotalPrice = item.selectedQuantity * item.price;
        return accumulator + itemTotalPrice;
    }, 0);
}

export const checkNotFoundError = (error: string): string => {
    return error.includes("CNF-001") ? "No customer record, please add" : error;
}

export const generateRandomAlphabetKey = () => {
    const timestamp = Date.now(); // Get current timestamp
    const randomNum = Math.floor(Math.random() * 1000); // Generate random number

    return `${timestamp}-${randomNum}`;
}
