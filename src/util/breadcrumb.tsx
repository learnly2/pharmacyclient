import React, {FC} from "react";
import {Breadcrumb} from "antd";

interface BreadcrumbComponentProps {
    currentPage: string
    handleBack: () => void
}

export const BreadcrumbComponent: FC<BreadcrumbComponentProps> = ({handleBack, currentPage}) => (
    <Breadcrumb>
        <Breadcrumb.Item onClick={handleBack}>Back</Breadcrumb.Item>
        <Breadcrumb.Item>{currentPage}</Breadcrumb.Item>
    </Breadcrumb>
);
