import React, {FC} from "react";
import {useAppDispatch} from "../redux/store/hooks";
import {Alert} from "antd";

interface ErrorComponentProps {
    dispatchFunction?: any
    params?: any
    error: string | undefined
    retry?: boolean
}

export const ApiErrorComponent: FC<ErrorComponentProps> = ({dispatchFunction, params, error, retry}) => {
    const dispatch = useAppDispatch();

    const handleRetry = () => {
        dispatch(dispatchFunction(params));
    };

    return (
        error ? <Alert message={error} type="error" showIcon /> : null
    )
};
