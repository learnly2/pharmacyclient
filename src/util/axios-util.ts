import axios, {AxiosError, AxiosResponse, InternalAxiosRequestConfig} from "axios";
import {logout, refreshTokenRequest, updateToken} from "../redux/slice/sign-in-slice";
import {TokenResponse} from "../schema/app-data-schema";

export const axiosInstance = axios.create({
    baseURL: `http://${window.location.hostname}:9347`,
    timeout: 10000,
    headers: {'Content-Type': 'application/json'}
});

let store: any;
export const injectStore = (_store: any) => {
    store = _store
}

axiosInstance.interceptors.request.use(
    (config) => {
        const {accessToken} = store.getState().signInReducer; // get the token from the Redux store
        const {api} = store.getState().publicApiListReducer;

        if (accessToken && !api.includes(String(config.url))) {
            config.headers.Authorization = `Bearer ${accessToken}`; // set the Authorization header of the request
        }
        return config;
    },
    (error) => Promise.reject(error)
);

const doRefreshToken = async (): Promise<TokenResponse> => {
    // Dispatch an action to initiate the token refresh request
    const {refreshToken} = store.getState().signInReducer;
    return refreshTokenRequest(refreshToken)
};

interface IRequestQueueItem {
    config: InternalAxiosRequestConfig;
    resolve: (value?: any) => void;
    reject: (reason?: any) => void;
}

let isRefreshing = false;
let requestQueue: IRequestQueueItem[] = [];

function processRequestQueue(error: AxiosError | null, token?: string) {
    requestQueue.forEach((item) => {
        if (error) {
            item.reject(error);
        } else {
            // @ts-ignore
            item.config.headers.Authorization = `Bearer ${token}`;
            axios.request(item.config)
                .then(item.resolve)
                .catch(item.reject);
        }
    });
    requestQueue = [];
}

axiosInstance.interceptors.response.use(
    (response: AxiosResponse) => {
        return response;
    },
    async (error: AxiosError) => {
        // @ts-ignore
        if (error.response?.status === 401 && !error.config.retry) {
            const {refreshToken} = store.getState().signInReducer;
            if (!isRefreshing) {
                isRefreshing = true;
                return doRefreshToken()
                    .then((tokenResponse) => {
                        store.dispatch(updateToken(tokenResponse))
                        processRequestQueue(null, String(tokenResponse.access_token));
                        // @ts-ignore
                        return axiosInstance.request(error.config);
                    })
                    .catch((error) => {
                        store.dispatch(logout({refreshToken: refreshToken}));
                        throw new Error(error.message);
                    })
                    .finally(() => {
                        isRefreshing = false;
                    });
            } else {
                return new Promise((resolve, reject) => {
                    // @ts-ignore
                    requestQueue.push({config: error.config, resolve, reject});
                });
            }
        } else {
            if (error.response) {
                // @ts-ignore
                throw new Error(error.response.data.errors || error.response.statusText);
            }
            throw new Error(error.message);
        }
    }
);
