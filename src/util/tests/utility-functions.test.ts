/**
 * @jest-environment jsdom
 */

import {
    calculateCartItemPrice,
    checkNotFoundError, currencyFormatter,
    decodeJwtToken,
    extractError,
    extractRolesFromTokenObject,
    extractUserIdFromTokenObject, filterItemFromArray, removeItemFromObjectList, updateStateOrCreate
} from "../helpers";
import {CartItem} from "../../schema/app-data-schema";

test('JWT decode', () => {
   let token: string = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJWeE0yOHd3cmZKR0NubExoaGVlVk51Q0c2TlhDWFAzLXFrQ1RPajhJRHI4In0.eyJleHAiOjE2ODE5ODc0ODgsImlhdCI6MTY4MTk4NjU4OCwianRpIjoiNTI4N2I2MjYtYmVmNS00N2M3LTgyODMtYjdjNjk1MWJkNmJlIiwiaXNzIjoiaHR0cDovLzAuMC4wLjA6ODA4MC9yZWFsbXMvZGVsaXZlcnkiLCJzdWIiOiIwNzBiM2U0MC1lMDE0LTQwYmMtOWE5ZC03NDFlYjk4ZGM2OTUiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJkZWxpdmVyeW10YWFuaSIsInNlc3Npb25fc3RhdGUiOiJjZjgwNGY3MS1iZjI5LTQzODItODc3Mi05MDNkOGFjNTljYWEiLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIlJPTEVfRFJJVkVSIl19LCJzY29wZSI6IiIsInNpZCI6ImNmODA0ZjcxLWJmMjktNDM4Mi04NzcyLTkwM2Q4YWM1OWNhYSJ9.Fc-1bEkyfXhPLEbybwcqnzJMqanlus9ekaAQ8ZrviWgdslVorlbvyP5qRsANM2wqwL5OI-4BPEpM8Ii-HHB3x1Cj5ZTkQP9FtapZ8tNcURaQpKvydp3k0OpxxmdpG-ZeQUo966EkP5DP9rpslTHDV77KReSVfzTPVUpo1Zb54c7wmtVOrnr2N_1E_RqXO8LXUJk6KPgal2Va_lOxHwbYMTImdP3_oV5sobd0W5LHj7a-UWgQeYKAUj9R5r2tc6G-a7pFIj7xZHYLhbRfyIsrZm2exiHILjdkBzseR3f0JIoBsJFXVAxT2SYl70leYrO2gAnKNg0eBPbnogqI3G5wVg";
    const decoded = decodeJwtToken(token);

   expect(decoded).toEqual({
        "exp": 1681987488,
        "iat": 1681986588,
        "jti": "5287b626-bef5-47c7-8283-b7c6951bd6be",
        "iss": "http://0.0.0.0:8080/realms/delivery",
        "sub": "070b3e40-e014-40bc-9a9d-741eb98dc695",
        "typ": "Bearer",
        "azp": "deliverymtaani",
        "session_state": "cf804f71-bf29-4382-8772-903d8ac59caa",
        "acr": "1",
        "realm_access": {
            "roles": [
                "ROLE_DRIVER"
            ]
        },
        "scope": "",
        "sid": "cf804f71-bf29-4382-8772-903d8ac59caa"
    })

   expect(extractUserIdFromTokenObject(decoded)).toEqual("070b3e40-e014-40bc-9a9d-741eb98dc695");
   expect(extractRolesFromTokenObject(decoded)).toEqual(['ROLE_DRIVER'])
});

test("Authentication error extraction", () => {
    let error:string = "{\"error\":\"invalid_grant\",\"error_description\":\"Invalid user credentials\"}"
    expect(extractError(error)).toEqual("Invalid user credentials");
})

describe('checkNotFoundError', () => {
    it('should return "No customer record, please add" if error includes "CNF-001"', () => {
        const error = "Error: CNF-001: Customer not found";
        const result = checkNotFoundError(error);
        expect(result).toBe("No customer record, please add");
    });

    it('should return the original error if error does not include "CNF-001"', () => {
        const error = "Error: CNF-002: Another error";
        const result = checkNotFoundError(error);
        expect(result).toBe(error);
    });
});

describe('calculateCartItemPrice', () => {
    it('should return the correct total price when given an array of cart items', () => {

        const items:CartItem[] = [
            { id: "aaaa", name: "Mara moja", quantity: 10, price: 10, exempt: false,selectedQuantity: 2},
            { id: "aaaa", name: "Hedex", quantity: 10, price: 10, exempt: false,selectedQuantity: 2},
            { id: "aaaa", name: "Panadol", quantity: 10, price: 10, exempt: false,selectedQuantity: 2},
        ];

        const totalPrice = calculateCartItemPrice(items);

        expect(totalPrice).toBe((2*10) *3);
    });

    it('should return 0 when given an empty array of cart items', () => {
        const items: CartItem[] = [];

        const totalPrice = calculateCartItemPrice(items);

        expect(totalPrice).toBe(0);
    });
});

describe('currencyFormatter', () => {
    it('should format the currency correctly', () => {
        const amount = 1234.5678;
        const formattedAmount = currencyFormatter.format(amount);

        expect(formattedAmount.replace(/\s/g, '')).toBe('Ksh1,234.57');
    });

    it('should format zero amount as "Ksh 0.00"', () => {
        const amount = 0;
        const formattedAmount = currencyFormatter.format(amount);

        expect(formattedAmount.replace(/\s/g, '')).toBe('Ksh0.00');
    });

    it('should format negative amount correctly', () => {
        const amount = -9876.54321;
        const formattedAmount = currencyFormatter.format(amount);

        expect(formattedAmount.replace(/\s/g, '')).toBe('-Ksh9,876.54');
    });
});

describe('filterItemFromArray', () => {
    it('should remove the specified item from the array', () => {
        const currentState = [1, 2, 3, 4, 5];
        const filterValue = 3;

        const result = filterItemFromArray(currentState, filterValue);

        expect(result).toEqual([1, 2, 4, 5]);
    });

    it('should return a new array without modifying the original array', () => {
        const currentState = [1, 2, 3, 4, 5];
        const filterValue = 3;

        const result = filterItemFromArray(currentState, filterValue);

        expect(result).toEqual([1, 2, 4, 5]);
        expect(result).not.toBe(currentState);
    });

    it('should return the same array if the filter value is not found', () => {
        const currentState = [1, 2, 3, 4, 5];
        const filterValue = 6;

        const result = filterItemFromArray(currentState, filterValue);

        expect(result).toEqual([1, 2, 3, 4, 5]);
        expect(result).toStrictEqual(currentState);
    });
});


describe('removeItemFromObjectList', () => {
    it('should remove items with the specified key and value from the object list', () => {
        const currentState = [
            { id: 1, name: 'Item 1' },
            { id: 2, name: 'Item 2' },
            { id: 3, name: 'Item 3' },
        ];
        const key = 'id';
        const value = 2;

        const result = removeItemFromObjectList(currentState, key, value);

        expect(result).toEqual([
            { id: 1, name: 'Item 1' },
            { id: 3, name: 'Item 3' },
        ]);
    });

    it('should return a new array without modifying the original object list', () => {
        const currentState = [
            { id: 1, name: 'Item 1' },
            { id: 2, name: 'Item 2' },
            { id: 3, name: 'Item 3' },
        ];
        const key = 'id';
        const value = 2;

        const result = removeItemFromObjectList(currentState, key, value);

        expect(result).toEqual([
            { id: 1, name: 'Item 1' },
            { id: 3, name: 'Item 3' },
        ]);
        expect(result).not.toBe(currentState);
    });

    it('should return the same array if no items match the key and value', () => {
        const currentState = [
            { id: 1, name: 'Item 1' },
            { id: 2, name: 'Item 2' },
            { id: 3, name: 'Item 3' },
        ];
        const key = 'id';
        const value = 4;

        const result = removeItemFromObjectList(currentState, key, value);

        expect(result).toEqual([
            { id: 1, name: 'Item 1' },
            { id: 2, name: 'Item 2' },
            { id: 3, name: 'Item 3' },
        ]);
        expect(result).toStrictEqual(currentState);
    });
});

describe('updateStateOrCreate', () => {
    it('should update existing item in the state', () => {
        const currentState = [
            { id: 1, name: 'Item 1', price: 10 },
            { id: 2, name: 'Item 2', price: 20 },
        ];
        const comparisonKey = 'id';
        const changeValues = ['name', 'price'];
        const payload = { id: 2, name: 'Updated Item 2', price: 30 };

        const result = updateStateOrCreate(currentState, comparisonKey, changeValues, payload);

        expect(result).toEqual([
            { id: 1, name: 'Item 1', price: 10 },
            { id: 2, name: 'Updated Item 2', price: 30 },
        ]);
    });

    it('should create a new item in the state', () => {
        const currentState = [
            { id: 1, name: 'Item 1', price: 10 },
            { id: 2, name: 'Item 2', price: 20 },
        ];
        const comparisonKey = 'id';
        const changeValues = ['name', 'price'];
        const payload = { id: 3, name: 'New Item', price: 30 };

        const result = updateStateOrCreate(currentState, comparisonKey, changeValues, payload);

        expect(result).toEqual([
            { id: 1, name: 'Item 1', price: 10 },
            { id: 2, name: 'Item 2', price: 20 },
            { id: 3, name: 'New Item', price: 30 },
        ]);
    });
});
