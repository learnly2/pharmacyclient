import React from 'react';
import {createBrowserRouter} from "react-router-dom";
import {ErrorPage} from "./util/error-page";
import {SignIn} from "./public/auth/sign-in";
import {Redirect} from "./core/redirect/redirect";
import {AuthenticatedRoute} from "./routes/AuthenticatedRoute";
import {CoreLayout} from "./core/core-layout";
import {AdminRoute} from "./routes/AdminRoute";
import {PublicLayout} from './public/public-layout';
import {Logout} from "./public/auth/logout";
import {AdminIndex} from "./core/admin/admin-index";
import {BillIndex} from "./core/admin/bills/bill-index";

export const router = createBrowserRouter([
    {
        path: "/",
        element: <PublicLayout/>,
        children: [
            {
                index: true,
                element: <SignIn/>,
            },
            {
                path: "redirect",
                element: <AuthenticatedRoute children={<Redirect/>}/>,
            },
            {
                path: "logout",
                element: <AuthenticatedRoute children={<Logout/>}/>,
            }
        ],
        errorElement: <ErrorPage/>,
    },
    {
        path: "/dashboard",
        element: <CoreLayout/>,
        children: [
            {
                path: "admin",
                element: <AuthenticatedRoute children={<AdminRoute children={<AdminIndex/>}/>}/>,
            }
        ],
        errorElement: <ErrorPage/>,
    },
    {
        path: "/admin",
        element: <CoreLayout/>,
        children: [
            {
                path: "bills",
                element: <AuthenticatedRoute children={<AdminRoute children={<BillIndex/>}/>}/>,
            }
        ],
        errorElement: <ErrorPage/>,
    }
]);
