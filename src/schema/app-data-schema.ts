interface CommonState {
    loading: boolean
    error: string | undefined
}

export interface SignInState extends CommonState {
    username: string
    password: string,
    roles: string[],
    accessToken: string,
    refreshToken: string,
    userId: string,
    loggedIn: boolean
}

export interface TokenResponse {
    access_token: string
    refresh_token: string
}

export interface TokenRequest {
    username: string
    password: string
}

export interface TokenData {
    userId: string,
    roles: string[]
}

export interface SideBarToggle {
    sideBarOn: boolean
    activeItem: string | undefined
}

export interface PublicApiList {
    api: string[]
}

export interface LogoutRequest {
    refreshToken: string
}

export interface RefreshTokenRequest extends LogoutRequest {
}

export interface Inventory {
    id: string
    name: string
    quantity: number,
    price: number,
    exempt: boolean
}

export interface InventoryItemsState extends CommonState {
    items: Inventory[]
}

export interface AddInventoryRequest {
    inventoryId: string
    name: string
    quantity: number
    price: number
}

export interface AddInventoryModalState extends CommonState {
    open: boolean
    inventoryId: string
    name: string
    quantity: number
    price: number | null
}

export interface UpdatedQuantity {
    selectedQuantity: number
    id: string
}

export interface CartItem extends Inventory {
    selectedQuantity: number
}

export interface CartState extends CommonState {
    items: CartItem[]
    openDrawer: boolean
    billId: string
    billComplete: boolean
}

export interface Customer {
    id: string
    fullName: string
    phoneNumber: string
}

export interface FetchCustomerRequest {
    phn: string
    id: string
}

export interface CustomerState extends CommonState {
    customer: Customer,
    customerId: string
}

export interface AddCustomerRequest {
    fullName: string
    phoneNumber: string
}

export interface CreateBillRequest {
    billId: string
    totalPrice: number
    customerId: string
    items: BillItemRequest[]
}

export interface BillItemRequest {
    quantity: number
    price: number
    inventoryId: string
}

export interface Bill {
    id: string
    totalPrice: number
    complete: boolean
    customerId: string
}

export interface BillItem {
    id: string
    quantity: number
    price: number
    exempt: boolean
    billId: string
    inventoryId: string
}

export interface CreateBillResponse {
    bill: Bill
    items: BillItem[]
}

export interface CreateBillState extends CommonState {
    bill: Bill
    items: BillItem[]
}

export interface ExtendedBill extends Bill {
    fullName: string
    createdAt: number
}

export interface BillListState extends CommonState {
    bills: ExtendedBill[]
}

export interface BillIdRequest {
    billId: string
}

export interface ExemptBillItemRequest extends BillIdRequest {
    inventoryId: string
}

export interface InventoryItemRequest {
    inventoryId: string
}
