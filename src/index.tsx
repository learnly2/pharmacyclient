import React from 'react';
import {createRoot} from 'react-dom/client';
import {Provider} from 'react-redux';
import {router} from './App';
import reportWebVitals from './reportWebVitals';
import './index.css';
import {ConfigProvider} from 'antd';
import { RouterProvider } from 'react-router-dom';
import {store} from "./redux/store/store";
import {injectStore} from "./util/axios-util";

const container = document.getElementById('root')!;
const root = createRoot(container);

root.render(
    <React.StrictMode>
        <ConfigProvider
            theme={{
                token: {
                    colorPrimary: '#52C41A',
                    "colorBgLayout": "#fafafa"
                },
            }}
        >
            <Provider store={store}>
                <RouterProvider router={router} />
            </Provider>
        </ConfigProvider>
    </React.StrictMode>
);

injectStore(store);
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
