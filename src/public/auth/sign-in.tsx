import {Col, Row, Typography} from "antd";
import {SignForm} from "./sign-form";
import {useAppSelector} from "../../redux/store/hooks";
import {useEffect} from "react";
import {useNavigate} from "react-router-dom";

const {Title, Paragraph} = Typography;
export const SignIn = () => {
    const {loggedIn} = useAppSelector(state => state.signInReducer);
    const navigate = useNavigate();

    useEffect(() => {
        if (loggedIn) {
            navigate("/redirect", {replace: true})
        }
    }, [loggedIn, navigate])

    return (
        <Row className={'sign-in-container'}>
            <Col
                sm={{span: 20, offset: 2}}
                xs={{span: 20, offset: 2}}
                md={{span: 16, offset: 4}}
                lg={{span: 8, offset: 8}}>
                <Title level={1}>Welcome to Pharma POC</Title>
                <Paragraph>Sign in to get access to your reports, status updates and much more.</Paragraph>
                <SignForm/>
            </Col>
        </Row>
    )
}
