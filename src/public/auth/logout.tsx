import {Spin} from "antd";
import {LoadingOutlined} from "@ant-design/icons";
import {useEffect} from "react";
import {useAppDispatch, useAppSelector} from "../../redux/store/hooks";
import {logout} from "../../redux/slice/sign-in-slice";
import {redirect} from "react-router-dom";

export const Logout = () => {
    const dispatch = useAppDispatch();
    const {refreshToken, loggedIn} = useAppSelector(state => state.signInReducer);

    useEffect(() => {
        if (loggedIn) {
            dispatch(logout({refreshToken: refreshToken}))
        } else {
            redirect("/")
        }
    }, [dispatch, refreshToken, loggedIn])

    return <Spin indicator={<LoadingOutlined style={{fontSize: 24}} spin/>}/>;
}
