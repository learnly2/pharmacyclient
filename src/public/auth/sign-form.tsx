import {Button, Form, Input, Spin} from "antd";
import {LockOutlined, MailOutlined} from "@ant-design/icons";
import {useAppDispatch, useAppSelector} from "../../redux/store/hooks";
import {errorInput, fetchToken} from "../../redux/slice/sign-in-slice";
import { ApiErrorComponent } from "../../util/api-error-component";

interface FormTypes {
    username: string
    password: string
}

export const SignForm = () => {
    const [form] = Form.useForm<FormTypes>();
    const dispatch = useAppDispatch();
    const {error, loading, username, password} = useAppSelector(state => state.signInReducer);

    const onFinish = ({username, password}: FormTypes) => {
        dispatch(fetchToken({
            username: username,
            password: password
        }))
    }

    const onFinishFailed = (errorInfo: any) => {
        dispatch(errorInput(errorInfo.message));
    };

    return (
        <Spin spinning={loading}>
            <ApiErrorComponent error={error}/>
            <br/>
            <Form
                initialValues={{username: username, password: password}}
                name="basic"
                layout={'vertical'}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
                form={form}
                size={'large'}>
                <Form.Item
                    name="username"
                    rules={[
                        {required: true, message: 'Please input your username!'},
                        {type: "email", message: "Invalid username!"},
                        {whitespace: false},
                        {max: 50, message: 'Maximum characters is 50!'}
                    ]}>
                    <Input
                        prefix={<MailOutlined className="site-form-item-icon"/>}
                        placeholder="Username"/>
                </Form.Item>

                <Form.Item
                    name="password"
                    rules={[
                        {required: true, message: 'Please input your password!'},
                        {type: "string", whitespace: false},
                        {min: 4, message: 'Minimum characters is 4!'},
                        {max: 30, message: 'Maximum characters is 30!'},
                    ]}>
                    <Input.Password
                        prefix={<LockOutlined className="site-form-item-icon"/>}
                        placeholder="Password"/>
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit" block={true}>
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </Spin>
    )
}
