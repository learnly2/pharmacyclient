import {Layout} from "antd";
import {Content} from "antd/es/layout/layout";
import { Outlet } from "react-router-dom";

export const PublicLayout = () => {
    return(
        <Layout>
            <Content>
                <Outlet/>
            </Content>
        </Layout>
    )
}
