import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {PublicApiList} from "../../../schema/app-data-schema";

const publicApiList: PublicApiList = {
    api: ['/api/v1/account/token/refresh']
}

const publicApiListSlice = createSlice({
    name: "publicApiList",
    initialState: publicApiList,
    reducers: {
        addPublicApi: (state, {payload}: PayloadAction<string>) => {
            state.api = [...state.api, payload]
        }
    }
});


export const {addPublicApi} = publicApiListSlice.actions;
export const publicApiListReducer = publicApiListSlice.reducer;
