import {createSlice} from "@reduxjs/toolkit";
import {PayloadAction} from "@reduxjs/toolkit/dist/createAction";
import {SideBarToggle} from "../../../schema/app-data-schema";

const sideBarToggleState: SideBarToggle = {
    sideBarOn: false,
    activeItem: ''
};

const sideBarToggleSlice = createSlice({
    name: 'sidebar',
    initialState: sideBarToggleState,
    reducers: {
        hydrateCollapsible: (state, {payload}: PayloadAction<void>) => {
            state.sideBarOn = !state.sideBarOn
        },
        handleTopNavActiveItem: (state, {payload}: PayloadAction<string | undefined>) => {
            state.activeItem = payload
        }
    },
    extraReducers: {}
});

export const {hydrateCollapsible, handleTopNavActiveItem} = sideBarToggleSlice.actions;
export const sideBarToggleReducer = sideBarToggleSlice.reducer;
