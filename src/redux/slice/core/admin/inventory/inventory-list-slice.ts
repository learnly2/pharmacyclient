import {createAsyncThunk, createSlice, isAnyOf} from "@reduxjs/toolkit";
import {Inventory, InventoryItemRequest, InventoryItemsState} from "../../../../../schema/app-data-schema";
import {axiosInstance} from "../../../../../util/axios-util";
import {extractError, removeItemFromObjectList, updateStateOrCreate} from "../../../../../util/helpers";
import {PayloadAction} from "@reduxjs/toolkit/dist/createAction";

export const fetchInventoryItems = createAsyncThunk<Inventory[], void, { rejectValue: string }>(
    'inventoryList/fetch',
    async () => {

        const response = await axiosInstance({
            method: 'GET',
            url: '/api/v1/inventory',
            data: {},
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    }
);

export const deleteInventoryItem = createAsyncThunk<string, InventoryItemRequest, { rejectValue: string }>(
    'inventoryList/delete',
    async (request) => {

        const response = await axiosInstance({
            method: 'DELETE',
            url: '/api/v1/inventory',
            data: request,
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    }
);

const inventoryListState: InventoryItemsState = {
    items: [],
    loading: false,
    error: ""
}
const inventoryListSlice = createSlice({
    name: "inventoryList",
    initialState: inventoryListState,
    reducers: {
        addInventoryItem: (state, {payload}: PayloadAction<Inventory>) => {
            state.items = updateStateOrCreate(state.items, 'id', ['name', 'price', 'quantity'], payload)
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchInventoryItems.fulfilled, (state, {payload}) => {
                state.loading = false;
                state.error = "";
                state.items = payload;
            })
            .addCase(deleteInventoryItem.fulfilled, (state, {payload}) => {
                state.loading = false;
                state.error = "";
                state.items = removeItemFromObjectList(state.items, 'id', payload)
            })
            .addMatcher(isAnyOf(fetchInventoryItems.pending, deleteInventoryItem.pending), (state) => {
                state.loading = true;
                state.error = "";
            })
            .addMatcher(isAnyOf(fetchInventoryItems.rejected, deleteInventoryItem.rejected), (state, {error}) => {
                state.error = extractError(String(error.message));
                state.loading = false;
            })
    }
});

export const {addInventoryItem} = inventoryListSlice.actions;
export const inventoryListReducer = inventoryListSlice.reducer;
