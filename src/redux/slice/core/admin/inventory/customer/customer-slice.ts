import {createAsyncThunk, createSlice, isAnyOf} from "@reduxjs/toolkit";
import {
    AddCustomerRequest,
    Customer,
    CustomerState,
    FetchCustomerRequest,
} from "../../../../../../schema/app-data-schema";
import {axiosInstance} from "../../../../../../util/axios-util";
import {extractError} from "../../../../../../util/helpers";
import {PayloadAction} from "@reduxjs/toolkit/dist/createAction";

export const fetchCustomer = createAsyncThunk<Customer, FetchCustomerRequest, { rejectValue: string }>(
    'customer/fetch',
    async ({phn, id}) => {

        const response = await axiosInstance({
            method: 'GET',
            url: `/api/v1/profile/customer?phn=${phn}&cid=${id}`,
            data: {},
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    }
);

export const addCustomer = createAsyncThunk<Customer, AddCustomerRequest, { rejectValue: string }>(
    'customer/add',
    async (request) => {

        const response = await axiosInstance({
            method: 'POST',
            url: `/api/v1/profile/customer`,
            data: request,
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    }
);

const defaultCustomer: Customer = {
    id: "",
    fullName: "",
    phoneNumber: ""
}

const customerState: CustomerState = {
    customer: defaultCustomer,
    loading: false,
    error: "",
    customerId: ""
}

const customerSlice = createSlice({
    name: "customer",
    initialState: customerState,
    reducers: {
        handleCustomerError: (state, {payload}: PayloadAction<string>) => {
            state.error = payload;
        },
        handleResetCustomer: (state) => {
            state.customer = defaultCustomer;
        },
        hydrateCustomerId: (state, {payload}: PayloadAction<string>) => {
            state.customerId = payload;
        }
    },
    extraReducers: (builder) => {
        builder
            .addMatcher(isAnyOf(fetchCustomer.fulfilled, addCustomer.fulfilled), (state, {payload}) => {
                state.loading = false;
                state.error = "";
                state.customer = payload;
            })
            .addMatcher(isAnyOf(fetchCustomer.pending, addCustomer.pending), (state) => {
                state.loading = true;
                state.error = "";
            })
            .addMatcher(isAnyOf(fetchCustomer.rejected, addCustomer.rejected), (state, {error}) => {
                state.error = extractError(String(error.message));
                state.loading = false;
            })
    }
});

export const {handleCustomerError, handleResetCustomer, hydrateCustomerId} = customerSlice.actions;
export const customerReducer = customerSlice.reducer;
