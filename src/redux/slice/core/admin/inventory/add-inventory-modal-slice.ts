import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {AddInventoryModalState, AddInventoryRequest, Inventory} from "../../../../../schema/app-data-schema";
import {PayloadAction} from "@reduxjs/toolkit/dist/createAction";
import {axiosInstance} from "../../../../../util/axios-util";
import {extractError} from "../../../../../util/helpers";

export const addInventoryItem = createAsyncThunk<Inventory, AddInventoryRequest, { rejectValue: string }>(
    'addInventoryModal/add/item',
    async (request) => {

        const response = await axiosInstance({
            method: 'POST',
            url: '/api/v1/inventory',
            data: request,
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    }
);

const addInventoryModalState: AddInventoryModalState = {
    loading: false,
    error: "",
    open: false,
    inventoryId: "",
    name: "",
    quantity: 1,
    price: null
}

const addInventoryModalSlice = createSlice({
    name: "addInventoryModal",
    initialState: addInventoryModalState,
    reducers: {
        handleInventoryModal: (state, {payload}: PayloadAction<boolean>) => {
            state.open = payload;
        },
        inventoryInputError: (state, {payload}: PayloadAction<string>) => {
            state.error = payload;
        },
        handleInventoryId: (state, {payload}: PayloadAction<string>) => {
            state.inventoryId = payload;
        },
        handleNameInput: (state, {payload}: PayloadAction<string>) => {
            state.name = payload;
        },
        handleQuantityInput: (state, {payload}: PayloadAction<number>) => {
            state.quantity = payload;
        },
        handlePriceInput: (state, {payload}: PayloadAction<number | null>) => {
            state.price = payload;
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(addInventoryItem.pending, (state) => {
                state.loading = true;
                state.error = "";
            })
            .addCase(addInventoryItem.fulfilled, (state, {payload}) => {
                state.loading = false;
                state.error = "";
                state.inventoryId = payload.id;
            })
            .addCase(addInventoryItem.rejected, (state, {error}) => {
                state.error = extractError(String(error.message));
                state.loading = false;
            })
    }
});

export const {
    handleInventoryModal,
    inventoryInputError,
    handleInventoryId,
    handleNameInput,
    handlePriceInput,
    handleQuantityInput
} = addInventoryModalSlice.actions;
export const addInventoryModalReducer = addInventoryModalSlice.reducer;
