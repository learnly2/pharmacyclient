import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {Bill, CreateBillRequest, CreateBillResponse, CreateBillState} from "../../../../../../schema/app-data-schema";
import {axiosInstance} from "../../../../../../util/axios-util";
import {extractError} from "../../../../../../util/helpers";

export const createBill = createAsyncThunk<CreateBillResponse, CreateBillRequest, { rejectValue: string }>(
    'createBill/add/bill',
    async (request) => {

        const response = await axiosInstance({
            method: 'POST',
            url: '/api/v1/finance/bill',
            data: request,
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    }
);

const billDefault: Bill = {
    id: "",
    totalPrice: 0,
    complete: false,
    customerId: ""
}

const createBillState: CreateBillState = {
    loading: false,
    error: "",
    bill: billDefault,
    items: []
}

const createBillSlice = createSlice({
    name: "createBill",
    initialState: createBillState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(createBill.pending, (state) => {
                state.loading = true;
                state.error = "";
            })
            .addCase(createBill.fulfilled, (state, {payload}) => {
                state.loading = false;
                state.error = "";
                state.bill = payload.bill;
                state.items = payload.items;
            })
            .addCase(createBill.rejected, (state, {error}) => {
                state.error = extractError(String(error.message));
                state.loading = false;
            })
    }
});

export const createBillReducer = createBillSlice.reducer;
