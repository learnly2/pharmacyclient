import {createAsyncThunk, createSlice, isAnyOf} from "@reduxjs/toolkit";
import {
    Bill,
    BillIdRequest, BillItem,
    CartItem,
    CartState, ExemptBillItemRequest,
    UpdatedQuantity
} from "../../../../../../schema/app-data-schema";
import {PayloadAction} from "@reduxjs/toolkit/dist/createAction";
import {extractError, removeItemFromObjectList, updateStateOrCreate} from "../../../../../../util/helpers";
import {axiosInstance} from "../../../../../../util/axios-util";

export const fetchBillItem = createAsyncThunk<CartItem[], BillIdRequest, { rejectValue: string }>(
    'cartItem/bill/items',
    async ({billId}) => {

        const response = await axiosInstance({
            method: 'GET',
            url: `/api/v1/finance/bill/items?bId=${billId}`,
            data: {},
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    }
);

export const completeBill = createAsyncThunk<Bill, BillIdRequest, { rejectValue: string }>(
    'cartItem/bill/complete',
    async (request) => {

        const response = await axiosInstance({
            method: 'PUT',
            url: `/api/v1/finance/bill`,
            data: request,
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    }
);

export const exemptBillItem = createAsyncThunk<BillItem, ExemptBillItemRequest, { rejectValue: string }>(
    'cartItem/bill/exempt',
    async (request) => {

        const response = await axiosInstance({
            method: 'PUT',
            url: `/api/v1/finance/bill/item`,
            data: request,
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    }
);

const cartState: CartState = {
    items: [],
    openDrawer: false,
    billId: "",
    loading: false,
    error: "",
    billComplete: false
}
const cartItemSlice = createSlice({
    name: "cartItem",
    initialState: cartState,
    reducers: {
        addCartItem: (state, {payload}: PayloadAction<CartItem>) => {
            state.items = updateStateOrCreate(state.items, 'id', [], payload);
        },
        removeCartItem: (state, {payload}: PayloadAction<string>) => {
            state.items = removeItemFromObjectList(state.items, 'id', payload);
        },
        updateCartItem: (state, {payload}: PayloadAction<UpdatedQuantity>) => {
            state.items = updateStateOrCreate(state.items, 'id', ['selectedQuantity'], payload);
        },
        handleCartDrawer: (state, {payload}: PayloadAction<boolean>) => {
            state.openDrawer = payload;
        },
        hydrateBillId: (state, {payload}: PayloadAction<string>) => {
            state.billId = payload;
        },
        resetCartItem: (state) => {
            state.items = [];
        },
        hydrateBillStatus: (state, {payload}: PayloadAction<boolean>) => {
            state.billComplete = payload;
        },
        hydrateItemError: (state, {payload}: PayloadAction<string>) => {
            state.error = payload;
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchBillItem.fulfilled, (state, {payload}) => {
                state.loading = false;
                state.error = "";
                state.items = payload;
            })
            .addCase(completeBill.fulfilled, (state, {payload}) => {
                state.loading = false;
                state.error = "";
                state.billComplete = payload.complete;
            })
            .addCase(exemptBillItem.fulfilled, (state, {payload}) => {
                state.loading = false;
                state.error = "";
                state.items = removeItemFromObjectList(state.items, 'id', payload);
            })
            .addMatcher(isAnyOf(fetchBillItem.pending, completeBill.pending, exemptBillItem.pending), (state) => {
                state.loading = true;
                state.error = "";
            })
            .addMatcher(isAnyOf(fetchBillItem.rejected, completeBill.rejected, exemptBillItem.rejected), (state, {error}) => {
                state.error = extractError(String(error.message));
                state.loading = false;
            })
    }
});

export const {
    addCartItem,
    removeCartItem,
    updateCartItem,
    handleCartDrawer,
    hydrateBillId,
    resetCartItem,
    hydrateBillStatus,
    hydrateItemError
} = cartItemSlice.actions;
export const cartItemReducer = cartItemSlice.reducer;
