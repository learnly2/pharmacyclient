import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {Bill, BillListState, ExtendedBill} from "../../../../../schema/app-data-schema";
import {axiosInstance} from "../../../../../util/axios-util";
import {extractError, updateStateOrCreate} from "../../../../../util/helpers";
import {PayloadAction} from "@reduxjs/toolkit/dist/createAction";

export const fetchAllBill = createAsyncThunk<ExtendedBill[], void, { rejectValue: string }>(
    'billList/fetch',
    async () => {

        const response = await axiosInstance({
            method: 'GET',
            url: '/api/v1/finance/bills',
            data: {},
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    }
);

const billListState: BillListState = {
    loading: false,
    error: "",
    bills: []
}

const billListSlice = createSlice({
    name: "billList",
    initialState: billListState,
    reducers: {
        handleCompleteBill: (state, {payload}: PayloadAction<Bill>) => {
            state.bills = updateStateOrCreate(state.bills, 'id', ['complete', 'totalPrice'], payload)
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchAllBill.pending, (state) => {
                state.loading = true;
                state.error = "";
            })
            .addCase(fetchAllBill.fulfilled, (state, {payload}) => {
                state.loading = false;
                state.error = "";
                state.bills = payload;
            })
            .addCase(fetchAllBill.rejected, (state, {error}) => {
                state.error = extractError(String(error.message));
                state.loading = false;
            })
    }
});

export const {handleCompleteBill} = billListSlice.actions;
export const billListReducer = billListSlice.reducer;
