import {createAsyncThunk, createSlice, isAnyOf, PayloadAction} from "@reduxjs/toolkit";
import {
    LogoutRequest,
    RefreshTokenRequest,
    SignInState,
    TokenRequest,
    TokenResponse
} from "../../schema/app-data-schema";
import {axiosInstance} from "../../util/axios-util";
import {
    extractError,
    extractTokenData
} from "../../util/helpers";
import {AppDispatch} from "../store/store";
import {addPublicApi} from "./public/public-api-list-slice";
import {Base64} from "js-base64";

export const fetchToken = createAsyncThunk<TokenResponse, TokenRequest, { rejectValue: string, dispatch: AppDispatch }>(
    'signIn/fetchToken',
    async (body, {dispatch}) => {

        const url = `/api/v1/account/admin/token`;
        dispatch(addPublicApi(url));

        const response = await axiosInstance({
            method: 'POST',
            url: url,
            data: body,
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    }
);

export const logout = createAsyncThunk<void, LogoutRequest, { rejectValue: string, dispatch: AppDispatch }>(
    'signIn/logout',
    async (body, {dispatch}) => {

        const url = `/api/v1/account/logout`;
        dispatch(addPublicApi(url));

        const response = await axiosInstance({
            method: 'POST',
            url: url,
            data: body,
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    }
);

export const refreshTokenRequest = (refreshToken: string) => {
    const refreshTokenRequest: RefreshTokenRequest = {
        refreshToken: refreshToken
    };

    return axiosInstance({
        method: 'POST',
        timeout: 2500,
        data: refreshTokenRequest,
        url: `/api/v1/account/token/refresh`,
        headers: {
            'Content-Type': 'application/json',
        }
    }).then((response) => response.data)
};

const signInAppState: SignInState = {
    error: "",
    loading: false,
    username: "",
    password: "",
    roles: [],
    accessToken: "",
    refreshToken: "",
    userId: "",
    loggedIn: false
}

const signInSlice = createSlice({
    name: "signIn",
    initialState: signInAppState,
    reducers: {
        passwordInput: (state, {payload}: PayloadAction<string>) => {
            state.password = payload;
        },
        usernameInput: (state, {payload}: PayloadAction<string>) => {
            state.username = payload;
        },
        errorInput: (state, {payload}: PayloadAction<string>) => {
            state.error = payload;
        },
        updateToken: (state, {payload}: PayloadAction<TokenResponse>) => {
            const {userId, roles} = extractTokenData(payload.access_token);
            state.userId = userId;
            state.roles = roles;
            state.accessToken = payload.access_token;
            state.refreshToken = payload.refresh_token;
            state.loggedIn = true;

            document.cookie = `${userId}=${Base64.encode(JSON.stringify({
                roles: roles,
                accessToken: payload.access_token,
                refreshToken: payload.refresh_token,
                userId: userId,
                loggedIn: true
            }))}; path=/; sameSite=${true};`;
            localStorage.setItem("ph_id", userId);
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchToken.rejected, (state, {error}) => {
                state.loggedIn = false;
                state.loading = false;
                state.error = extractError(String(error.message));
            })
            .addCase(fetchToken.pending, (state) => {
                state.loading = true;
                state.error = "";
            })
            .addCase(fetchToken.fulfilled, (state, {payload}) => {
                state.loading = false;
                state.error = "";
                const {userId, roles} = extractTokenData(payload.access_token);
                state.userId = userId;
                state.roles = roles;
                state.accessToken = payload.access_token;
                state.refreshToken = payload.refresh_token;
                state.loggedIn = true;

                document.cookie = `${userId}=${Base64.encode(JSON.stringify({
                    roles: roles,
                    accessToken: payload.access_token,
                    refreshToken: payload.refresh_token,
                    userId: userId,
                    loggedIn: true
                }))}; path=/; sameSite=${true};`;
                localStorage.setItem("ph_id", userId);
            })
            .addMatcher(isAnyOf(logout.pending, logout.fulfilled, logout.rejected), (state) => {
                document.cookie = `${state.userId}=; path=/; sameSite=${true};`;
                localStorage.setItem("ph_id", "");
                state.userId = "";
                state.roles = [];
                state.accessToken = "";
                state.refreshToken = "";
                state.loggedIn = false;
            });
    }
})

export const {passwordInput, usernameInput, errorInput, updateToken} = signInSlice.actions;
export const signInReducer = signInSlice.reducer;
