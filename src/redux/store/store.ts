import {configureStore, ThunkAction, Action} from '@reduxjs/toolkit';
import {signInReducer} from "../slice/sign-in-slice";
import {sideBarToggleReducer} from "../slice/core/sidebar-toggle-slice";
import {publicApiListReducer} from "../slice/public/public-api-list-slice";
import {getCookie} from "../../util/helpers";
import {inventoryListReducer} from "../slice/core/admin/inventory/inventory-list-slice";
import {addInventoryModalReducer} from "../slice/core/admin/inventory/add-inventory-modal-slice";
import {
    updateBillMiddleware,
    updateCompleteBillMiddleware,
    updateInventoryListMiddleware
} from "../middleware/middleware";
import {customerReducer} from "../slice/core/admin/inventory/customer/customer-slice";
import {createBillReducer} from "../slice/core/admin/inventory/cart/create-bill-slice";
import {billListReducer} from "../slice/core/admin/bills/bill-list-slice";
import {cartItemReducer} from "../slice/core/admin/inventory/cart/cart-item-slice";

const cookieJson = getCookie(localStorage.getItem('ph_id'));

const preloadedState = {
    signInReducer: {
        error: "",
        loading: false,
        username: "",
        password: "",
        roles: cookieJson?.roles || [],
        accessToken: cookieJson?.accessToken || "",
        refreshToken: cookieJson?.refreshToken || "",
        userId: cookieJson?.userId || "",
        loggedIn: cookieJson?.loggedIn || ""
    }
};

export const store = configureStore({
    reducer: {
        signInReducer: signInReducer,
        sideBarToggleReducer: sideBarToggleReducer,
        publicApiListReducer: publicApiListReducer,
        inventoryListReducer: inventoryListReducer,
        addInventoryModalReducer: addInventoryModalReducer,
        cartItemReducer: cartItemReducer,
        customerReducer: customerReducer,
        createBillReducer: createBillReducer,
        billListReducer: billListReducer
    },
    preloadedState: preloadedState,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware()
            .concat(updateInventoryListMiddleware)
            .concat(updateBillMiddleware)
            .concat(updateCompleteBillMiddleware)
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType,
    RootState,
    unknown,
    Action<string>>;
