import {Middleware} from "@reduxjs/toolkit";
import {AppDispatch} from "../store/store";
import {addInventoryItem, fetchInventoryItems} from "../slice/core/admin/inventory/inventory-list-slice";
import {hydrateBillId} from "../slice/core/admin/inventory/cart/cart-item-slice";
import {handleCompleteBill} from "../slice/core/admin/bills/bill-list-slice";

export const updateInventoryListMiddleware: Middleware = ({dispatch}: { dispatch: AppDispatch }) => next => action => {
    if (action.type === 'addInventoryModal/add/item/fulfilled') {
        dispatch(addInventoryItem(action.payload));
    }
    return next(action);
};

export const updateBillMiddleware: Middleware = ({dispatch}: { dispatch: AppDispatch }) => next => action => {
    if (action.type === 'createBill/add/bill/fulfilled') {
        dispatch(hydrateBillId(action.payload.bill.id));
    }
    return next(action);
};

export const updateCompleteBillMiddleware: Middleware = ({dispatch}: { dispatch: AppDispatch }) => next => action => {
    if (action.type === 'cartItem/bill/complete/fulfilled') {
        dispatch(handleCompleteBill(action.payload));
        dispatch(fetchInventoryItems());
    }
    return next(action);
};
